output "web_address" {
  value = "Web Address: http://${var.domain_name}"
}

output "ec2_machine" {
  value = "EC2 Private IP: ${aws_instance.helloworld_server.private_ip}"
}

output "load_balancer" {
  value = "Load Balancer DNS: ${aws_elb.public.dns_name}"
}